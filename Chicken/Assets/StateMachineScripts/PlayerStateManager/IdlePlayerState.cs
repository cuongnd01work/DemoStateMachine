﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdlePlayerState : PlayerState
{
    private GameManager _gameManager;

    public IdlePlayerState(GameManager gameManager)
    {
        _gameManager = gameManager;
    }

    public override void Enter(Animator animator)
    {
        animator.Play("Idle");

    }
}
