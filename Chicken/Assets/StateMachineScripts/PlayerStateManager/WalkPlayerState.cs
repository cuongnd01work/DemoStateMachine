﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkPlayerState : PlayerState
{
    private float _movementSpeed = 5;
    private Rigidbody2D _rigidbody2d;
    private SpriteRenderer _spriteRenderer;
    private Vector2 _movementVector;
    private GameManager _gameManager;

    public WalkPlayerState(GameManager gameManager)
    {
        _gameManager = gameManager;
    }

    public override void Enter(Animator animator)
    {
        animator.Play("Walk");
        if (_rigidbody2d == null)
        {
            _rigidbody2d = animator.GetComponent<Rigidbody2D>();
        }
        if (_spriteRenderer == null)
        {
            _spriteRenderer = animator.GetComponent<SpriteRenderer>();
        }
    }
    public override void Update(object options)
    {
        _movementVector = (Vector2)options;
        _rigidbody2d.velocity = _movementVector * _movementSpeed;
        if (Mathf.Abs(_movementVector.x) > 0.1f)
            _spriteRenderer.flipX = Vector3.Dot(Vector2.right, _movementVector) < 0;
    }
    public override void Exit()
    {
        _rigidbody2d.velocity = new Vector2(0,0);
    }
}
