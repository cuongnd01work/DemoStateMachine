﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateManager 
{
    public PlayerStateMachine Machine;
    public PlayerState IdlePlayerState { get; private set; }
    public PlayerState WalkPlayerState { get; private set; }
    public PlayerState AttackPlayerState { get; private set; }
    public PlayerStateManager(GameManager gameManager)
    {
        Machine = new PlayerStateMachine(gameManager);
        IdlePlayerState = new IdlePlayerState(gameManager);
        WalkPlayerState = new WalkPlayerState(gameManager);
        AttackPlayerState = new AttackPlayerState(gameManager);
        Machine.CurrentState = IdlePlayerState;
    }
}