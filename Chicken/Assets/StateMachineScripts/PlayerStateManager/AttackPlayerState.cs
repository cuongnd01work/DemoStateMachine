﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayerState : PlayerState
{
    private GameManager _gameManager;

    public AttackPlayerState(GameManager gameManager)
    {
        _gameManager = gameManager;
    }

    public override void Enter(Animator animator)
    {
        animator.Play("Attack");
    }
}
