﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameStateManger GameStateManger;
    public PlayerStateManager PlayerStateManager;
    public float TimePlay;
    public UIManager UIManager => uIManager;
    public GameObject Food => food;
    public Animator PlayerAnimator => playerAnimator;

    [SerializeField] private UIManager uIManager;
    [SerializeField] private GameObject food;
    [SerializeField] private Animator playerAnimator ;
    private void Start()
    {
        GameStateManger = new GameStateManger(this);
        PlayerStateManager = new PlayerStateManager(this);
    }
    private void Update()
    {
        PlayerStateManager?.Machine?.Update();
        GameStateManger?.Machine?.CurrentState?.Update();
    }

    public void Hide(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }
    public void Show(GameObject gameObject)
    {
        gameObject.SetActive(true);
    }
}
