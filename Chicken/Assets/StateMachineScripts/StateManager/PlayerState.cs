﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerState
{
    public virtual void Enter(Animator animator)
    {

    }
    
    public virtual void Update() { }
    public virtual void Update(object options) { }
    public virtual void Exit()
    {

    }
}
