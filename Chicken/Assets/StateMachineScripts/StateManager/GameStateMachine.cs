﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateMachine
{
    public GameState CurrentState;
    private GameManager _gameManager;

    public GameStateMachine(GameManager gameManager)
    {
        _gameManager = gameManager;
    }

    public void ChangeState(GameState state)
    {
        CurrentState.Exit();
        CurrentState = state;
        CurrentState.Enter();
    }
}
