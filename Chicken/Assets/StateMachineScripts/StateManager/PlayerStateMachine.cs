﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine 
{
    private Animator _animator;
    private Vector2 _movementInputVector;
    private GameManager _gameManager;
    public PlayerState CurrentState;
    public PlayerStateMachine(GameManager gameManager)
    {
        _gameManager = gameManager;
        _animator = _gameManager.PlayerAnimator;
    }
    public void ChangeState(PlayerState state)
    {
        CurrentState.Exit();
        CurrentState = state;
        CurrentState.Enter(_animator);
    }
    public void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ChangeState(_gameManager.PlayerStateManager.AttackPlayerState);
        }
        else if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            int inputHorizontal = (int)Input.GetAxisRaw("Horizontal");
            int inputVertical = (int)Input.GetAxisRaw("Vertical");
            _movementInputVector = new Vector2(inputHorizontal, inputVertical);
            _movementInputVector.Normalize();
            ChangeState(_gameManager.PlayerStateManager.WalkPlayerState);
            CurrentState.Update(_movementInputVector);
        }
        else
        {
            ChangeState(_gameManager.PlayerStateManager.IdlePlayerState);
        }
    }
}
