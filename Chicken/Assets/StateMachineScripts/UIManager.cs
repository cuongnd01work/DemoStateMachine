﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public PanelStart PanelStart { get => panelStart; }
    public PanelPlay PanelPlay { get => panelPlay; }
    public PanelOver PanelOver { get => panelOver; }
    public PanelWin PanelWin { get => panelWin; }

    [SerializeField] private PanelStart panelStart;
    [SerializeField] private PanelPlay panelPlay;
    [SerializeField] private PanelOver panelOver;
    [SerializeField] private PanelWin panelWin;
}
