﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverState : GameState
{
    private GameManager _gameManager;
    private PanelOver _panelOver;

    public GameOverState(GameManager gameManager)
    {
       _gameManager = gameManager;
        _panelOver = _gameManager.UIManager.PanelOver;
    }
    public override void Enter()
    {
        _gameManager.Show(_panelOver.gameObject);
        RegisterEvents();
    }
    public override void Exit()
    {
        RemoveEvents();
        _gameManager.Hide(_panelOver.gameObject);
    }
    private void RegisterEvents()
    {
        _panelOver.ButtonRestart.onClick.AddListener(RestartGame);
    }
    private void RemoveEvents()
    {
        _panelOver.ButtonRestart.onClick.RemoveAllListeners();
    }
    private void RestartGame()
    {
        GameState currentState = _gameManager.GameStateManger.InGameState;
        _gameManager.GameStateManger.Machine.ChangeState(currentState);
        _gameManager.PlayerAnimator.gameObject.transform.position = new Vector2(0, 0);
    }
}
