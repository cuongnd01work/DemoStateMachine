﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFinishState : GameState
{
    private GameManager _gameManager;
    private PanelWin _panelWin;

    public GameFinishState(GameManager gameManager)
    {
        _gameManager = gameManager;
        _panelWin = _gameManager.UIManager.PanelWin;
    }
    public override void Enter()
    {
        _gameManager.Show(_panelWin.gameObject);
        RegisterEvents();
    }
    public override void Exit()
    {
        RemoveEvents();
        _gameManager.Hide(_panelWin.gameObject);
    }
    private void RegisterEvents()
    {
        _panelWin.ButtonRestart.onClick.AddListener(RestartGame);
    }
    private void RemoveEvents()
    {
        _panelWin.ButtonRestart.onClick.RemoveAllListeners();
    }
    private void RestartGame()
    {
        _gameManager.GameStateManger.Machine.ChangeState(_gameManager.GameStateManger.InGameState);

        _gameManager.PlayerAnimator.gameObject.transform.position = new Vector2(0, 0);
    }
}
