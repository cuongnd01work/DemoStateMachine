﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManger
{
    public GameStateMachine Machine { get; private set; }
    public GameState GameStartState { get; private set; }
    public GameState InGameState { get; private set; }
    public GameState GameFinishState { get; private set; }
    public GameState GameOverState { get; private set; }

    public GameStateManger(GameManager gameManager)
    {
        Machine = new GameStateMachine(gameManager);
        GameStartState = new GameStartState(gameManager);
        InGameState = new InGameState(gameManager);
        GameFinishState = new GameFinishState(gameManager);
        GameOverState = new GameOverState(gameManager);
        Machine.CurrentState = GameStartState;
        Machine.ChangeState(GameStartState);
    }
}

