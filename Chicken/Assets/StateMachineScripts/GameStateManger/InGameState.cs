﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameState : GameState
{
    private GameManager _gameManager;
    private Text _timeText;

    private float timeSwamp = 0.3f;
    private float _lostTime;
    private float _lostNewTime = 0.000002f;
    public InGameState(GameManager gameManager)
    {
        _gameManager = gameManager;
        _timeText = _gameManager.UIManager.PanelPlay.TextTime;
    }
    public override void Enter()
    {
        _gameManager.TimePlay = 5;
        _lostTime = 0.003f;
        Time.timeScale = 1;
        _gameManager.Show(_gameManager.UIManager.PanelPlay.gameObject);
        _gameManager.StartCoroutine(createFood());
        _gameManager.PlayerAnimator.gameObject.transform.localScale = new Vector2(1, 1);
    }
    public override void Update()
    {
        _lostTime += _lostNewTime;
        _gameManager.TimePlay -= _lostTime;
        _timeText.text = Math.Round(_gameManager.TimePlay, 1) + "";
        if (_gameManager.TimePlay <= 0)
        {
            _gameManager.PlayerAnimator.gameObject.transform.localScale = new Vector2(50, 50);
            _gameManager.GameStateManger.Machine.ChangeState(_gameManager.GameStateManger.GameOverState);
        }
        else if (_gameManager.TimePlay > 15)
        {
            _gameManager.PlayerAnimator.gameObject.transform.localScale = new Vector2(50, 50);
            _gameManager.GameStateManger.Machine.ChangeState(_gameManager.GameStateManger.GameFinishState);
        }
    }
    public override void Exit()
    {
        _gameManager.StopAllCoroutines();
    }
    private IEnumerator createFood()
    {
        yield return new WaitForSeconds(timeSwamp);
        float randomXPosition = UnityEngine.Random.Range(-8f, 8f);
        float randomYPosition = UnityEngine.Random.Range(-4f, 4f);
        MonoBehaviour.Instantiate(_gameManager.Food, new Vector2(randomXPosition, randomYPosition), Quaternion.identity);
        _gameManager.StartCoroutine(createFood());
    }
}
