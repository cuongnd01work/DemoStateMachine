﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStartState : GameState
{
    private GameManager _gameManager;
    private PanelStart _panelStart;
    public GameStartState(GameManager gameManager)
    {
        _gameManager = gameManager;
        _panelStart = _gameManager.UIManager.PanelStart;
    }
    public override void Enter()
    {
        _gameManager.Show(_panelStart.gameObject);
        RegisterEvents();
    }
    public override void Exit()
    {
        _gameManager.Hide(_panelStart.gameObject);
        RemoveEvents();
    }
    private void RegisterEvents()
    {
        _panelStart.ButtonStart.onClick.AddListener(StartGame);
    }
    private void RemoveEvents()
    {
        _panelStart.ButtonStart.onClick.RemoveAllListeners();
    }
    private void StartGame()
    {
        _gameManager.GameStateManger.Machine.ChangeState(_gameManager.GameStateManger.InGameState);
    }
}
